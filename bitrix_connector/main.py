from time import sleep
import requests

class BitrixConnector:
    """
    Коннектор к битриксу
    На вход - портал
    """
    _portal = None

    def __init__(self, portal):
        self._portal = portal

    def get_webhook_url(self):
        return f'https://{self._portal.hostname}.bitrix24.ru/rest/{self._portal.api_user_id}/{self._portal.api_key}/'

    def call_method(self, method: str, data: dict = {}):
        """ Вызов переданного REST метода"""
        try:
            url = self.get_webhook_url() + method

            if method.rsplit('.', 1)[1] in ['add', 'update', 'delete', 'set']:
                r = requests.post(url, json=data).json()
            else:
                r = requests.post(url, data=data).json()

        except ValueError:
            if r['error'] not in 'QUERY_LIMIT_EXCEEDED':
                raise BitrixError(r)
            # Looks like we need to wait until expires limitation time by Bitrix24 API
            sleep(2)
            return self.call_method(method, data)

        if 'error' in r:
            raise BitrixError(r)
        if 'start' not in data:
            data['start'] = 0
        if 'next' in r and r['total'] > data['start']:
            data['start'] += 50
            data = self.call_method(method, data)
            if isinstance(r['result'], dict):
                result = r['result'].copy()
                result.update(data)
            else:
                result = r['result'] + data
            return result
        return r['result']

    def add_bx_task_to_bitrix(self, bx_task, task_keeper):
        """
        Добавляет задачу в битрикс, возвращает ID
        """
        fields = self.get_tasks_fields(bx_task)
        fields['OPENED'] = "Y"
        fields["CREATED_BY"] = task_keeper.createdBy.bitrix_id
        fields["RESPONSIBLE_ID"] = task_keeper.responsible.bitrix_id
        fields['AUDITORS'] = task_keeper.get_auditors_ids()
        fields['ACCOMPLICES'] = task_keeper.get_accomplices_ids()

        if task_keeper.auditors:
            fields['GROUP_ID'] = task_keeper.group.bitrix_id

        if task_keeper.group:
            fields['GROUP_ID'] = task_keeper.group.bitrix_id

        data = {
            'fields': fields
        }

        response = self.call_method("tasks.task.add", data)
        return response['task']

    def update_task(self, bx_task, task_keeper):
        """
        Обновляет задачу в битрикс, возвращает ID
        """
        fields = self.get_tasks_fields(bx_task)
        data = {
            'taskId': task_keeper.bitrix_id,
            'fields': fields,
        }

        response = self.call_method("tasks.task.update", data)
        return response['task']

    def update_task_custom_fields(self, task_id, field_value_dict):
        """
        Обновляет поля в задаче битрикс24, возвращает ID
        """
        data = {
            'taskId': task_id,
            'fields': field_value_dict,
        }

        response = self.call_method("tasks.task.update", data)
        return response['task']

    def get_leads_statuses(self):
        """
        Получает из битрикса ВСЕ ВОЗМОЖНЫЕ СТАТУСЫ ЛИДОВ
        :return:
        """
        data = {"filter": {"ENTITY_ID": "STATUS"}}
        response = self.call_method("crm.status.list", data)

        return response

    def get_leads_sources(self):
        """
        Получает из битрикса ВСЕ ВОЗМОЖНЫЕ ИСТОЧНИКИ ЛИДОВ
        :return:
        """
        data = {"filter": {"ENTITY_ID": "SOURCE"}}
        response = self.call_method("crm.status.list", data)

        return response

    def get_django_task_from_bitrix(self, task):
        """
        Получает из битрикса ЗАДАЧУ
        """
        data = {"id": task.bitrix_id}
        response = self.call_method('tasks.task.get', data)

        return response

    def get_task_from_bitrix(self, task_id):
        """
        Получает из битрикса задачу по bitrix_id
        """
        data = {"id": task_id}
        response = self.call_method('tasks.task.get', data)

        return response['task']

    def get_status_info(self, status_id: str):
        """
        Получает из битрикса информацию о статусе
        :param status_id: str
        :return:
        """
        data = {
            "filter": {
                "ENTITY_ID": "STATUS",
                "STATUS_ID": status_id
            }}

        self.call_method('crm.status.list', data)

        return self._response['result'][0]

    def get_groups(self):
        """
        Получает из битрикса все группы
        :return:
        """
        data = {}
        response = self.call_method("sonet_group.get", data)

        return response

    def get_tasks(self, group_id=""):
        """
        Получает из битрикса все задачи портала
        на вход ID Группы, если нужны задачи одной группы
        :return:
        """
        data = {}
        if group_id:
            data = {
                "filter[=GROUP_ID]": group_id,
            }
            # }

        response = self.call_method("tasks.task.list", data)

        return response['tasks']

    def get_users(self):
        """
        Получает из битрикса всех пользователей
        :return:
        """
        response = self.call_method("user.get", {})

        return response

    def get_tasks_fields(self, bx_task):
        fields = {
            'TITLE': bx_task['title'],
            'CREATED_DATE': bx_task['createdDate'],
        }

        if bx_task['description']:
            fields['DESCRIPTION'] = bx_task['description']

        if bx_task['status']:
            fields['STATUS'] = bx_task['status']

        if bx_task['startDatePlan']:
            fields['START_DATE_PLAN'] = bx_task['startDatePlan']

        if bx_task['deadline']:
            fields['DEADLINE'] = bx_task['deadline']

        if bx_task['timeEstimate']:
            fields['TIME_ESTIMATE'] = bx_task['timeEstimate']

        if bx_task['closedDate']:
            fields['CLOSED_DATE'] = bx_task['closedDate']
        return fields

    def get_comment(self, task_id, comment_id):
        data = {
            "TASKID": task_id,
            "ITEMID": comment_id,
        }

        response = self.call_method("task.commentitem.get", data)
        return response

    def add_comment(self, task_id, author_id, message):
        data = {
            "TASKID": task_id,
            "fields": {
                'POST_MESSAGE': message,
                'AUTHOR_ID': author_id,
            }
        }
        response = self.call_method("task.commentitem.add", data)
        return response

    def update_comment(self, task_id, comment_id, message):
        data = {
            "TASKID": task_id,
            'ITEMID': comment_id,
            "fields": {
                'POST_MESSAGE': message,
            }
        }
        response = self.call_method("task.commentitem.update", data)
        return response

    def get_elapsed_time(self, task_id):
        data = {
            "TASKID": task_id,
        }

        response = self.call_method("task.elapseditem.getlist", data)
        return response

    def add_elapsed_time(self, task_id, fields):
        data = {
            "TASKID": task_id,
            'fields': fields
        }

        response = self.call_method("task.elapseditem.add", data)
        return response

    def update_elapsed_time(self, task_id, time_id, fields):
        data = {
            "TASKID": task_id,
            'ITEMID': time_id,
            'fields': fields
        }

        response = self.call_method("task.elapseditem.add", data)
        return response

    def delete_elapsed_time(self, task_id, time_id):
        data = {
            "TASKID": task_id,
            'ITEMID': time_id,
        }
        response = self.call_method("task.elapseditem.delete", data)
        return response

class BitrixError(ValueError):
    def __init__(self, response):
        super().__init__(response['error_description'])
        self.code = response['error']
        self.description = response['error_description']

    def __str__(self):
        return f'Ошибка соединения с BX24, код: {self.code}. Описание: {self.description}'

