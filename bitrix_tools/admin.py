from django.contrib import admin, messages
from django.http import HttpResponseRedirect
from django.urls import path

from bitrix_connector.main import BitrixError
from .models import BitrixPortal, UserBitrix, GroupBitrix, Task, TaskExchanger, BitrixTaskKeeper, CustomLogic


class CustomExchangerLogicInline(admin.StackedInline):
    model = CustomLogic


class BitrixPortalAdmin(admin.ModelAdmin):

    change_form_template = "admin/bitrix_tools/bitrixportal_change_form.html"

    def change_view(self, request, object_id, form_url='', extra_context=None):
        instance = self.get_object(request, object_id)
        current_site = request.META['HTTP_HOST']
        extra_context = extra_context or {}
        extra_context['current_site'] = current_site
        extra_context['groups'] = instance.groupbitrix_set.all()
        extra_context['tasks'] = instance.bitrixtaskkeeper_set.all()
        extra_context['portal_users'] = instance.userbitrix_set.all()
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def response_change(self, request, obj):
        obj.save()
        try:
            if "_import-groups" in request.POST:
                obj.import_groups_from_bitrix()
                self.message_user(request, "Группы импортированы из Bitrix24!")
                return HttpResponseRedirect(".")

            if "_import-users" in request.POST:
                obj.import_users_from_bitrix()
                self.message_user(request, "Пользователи импортированы из Bitrix24!")
                return HttpResponseRedirect(".")

            if "_import-tasks-group" in request.POST:
                if 'task-import-group-id' in request.POST:
                    group_id = request.POST['task-import-group-id']
                    obj.import_tasks_from_bitrix(group_id)
                    self.message_user(request, "Задачи импортированы из Bitrix24!")
                else:
                    self.message_user(request, "Не выбрана группа!", level=messages.ERROR)
                return HttpResponseRedirect(".")
        except BitrixError as e:
            self.message_user(request, e, level=messages.ERROR)

        return super().response_change(request, obj)


class GroupBitrixAdmin(admin.ModelAdmin):
    fields = ('bitrix_portal', 'title', 'bitrix_id', 'show_bitrix_url')
    readonly_fields = ('show_bitrix_url', )


class TaskAdmin(admin.ModelAdmin):
    # readonly_fields = ('show_bitrix_url', )

    change_form_template = "admin/bitrix_tools/task_change_form.html"

    def change_view(self, request, object_id, form_url='', extra_context=None):
        instance = self.get_object(request, object_id)
        extra_context = extra_context or {}
        extra_context['related_tasks'] = instance.related.all()
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def response_change(self, request, obj):
        obj.save()
        if "_add_to_bitrix_portal" in request.POST:
            obj.add_task_to_bitrix_portal()
            obj.save()
            self.message_user(request, "Задача добавлена в Bitrix24")
            return HttpResponseRedirect(".")

        return super().response_change(request, obj)


class TaskExchangerAdmin(admin.ModelAdmin):
    inlines = [CustomExchangerLogicInline,]
    change_form_template = "admin/bitrix_tools/taskexchanger_change_form.html"

    fieldsets = (
        ("Портал 1", {
            'fields': ('bitrix_1', 'group_1', 'responsible_1', 'auditors_1'),
        }),
        ('Портал 2', {
            'classes': ('collapse',),
            'fields': ('bitrix_2', 'group_2', 'responsible_2', 'auditors_2'),
        }),
        ('Дополнительные настройки', {
            'classes': ('collapse',),
            'fields': ('enabled', 'save_shared_tasks', 'write_logs',),
        }),
    )

    def change_view(self, request, object_id, form_url='', extra_context=None):
        instance = self.get_object(request, object_id)
        current_site = request.META['HTTP_HOST']
        extra_context = extra_context or {}
        extra_context['current_site'] = current_site
        extra_context['logger'] = instance.logger_exchanger.order_by('-time')[:20]
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


admin.site.register(BitrixPortal, BitrixPortalAdmin)
admin.site.register(UserBitrix)
admin.site.register(GroupBitrix, GroupBitrixAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(TaskExchanger, TaskExchangerAdmin)
admin.site.register(BitrixTaskKeeper)