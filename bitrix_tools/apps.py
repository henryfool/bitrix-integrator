from django.apps import AppConfig


class BitrixToolsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bitrix_tools'
