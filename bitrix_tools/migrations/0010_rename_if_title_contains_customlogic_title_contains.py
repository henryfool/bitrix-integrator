# Generated by Django 3.2.7 on 2021-09-26 10:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bitrix_tools', '0009_customlogic'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customlogic',
            old_name='if_title_contains',
            new_name='title_contains',
        ),
    ]
