# Generated by Django 3.2.7 on 2021-09-24 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bitrix_tools', '0006_bitrixportal_related_task_url_field'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskexchanger',
            name='auditors_1',
            field=models.ManyToManyField(related_name='exchanger_auditors_1', to='bitrix_tools.UserBitrix', verbose_name='Наблюдатели для задач портала 1'),
        ),
        migrations.AddField(
            model_name='taskexchanger',
            name='auditors_2',
            field=models.ManyToManyField(related_name='exchanger_auditors_2', to='bitrix_tools.UserBitrix', verbose_name='Наблюдатели для задач портала 2'),
        ),
    ]
