from django.urls import path
from .views import *

urlpatterns = [
    path('portal/<int:pk>/', PortalDetailView.as_view(), name='portal-detail'),
    path('income/<str:hostname>/', income_webhook, name='income_webhook'),
]