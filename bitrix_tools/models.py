from django.contrib import admin
from django.db import models
from django.utils.html import format_html

from bitrix_connector.main import BitrixConnector
from django.utils.dateparse import parse_datetime, parse_duration
from django.urls import reverse
from django.utils import timezone
# Create your models here.


class BitrixPortal(models.Model):
    name = models.CharField(max_length=150, null=True, blank=True, verbose_name="Наименование портала Bitrix24")

    hostname = models.CharField(max_length=150, null=True, blank=True, verbose_name="Домен Bitrix24",
                                help_text='находится МЕЖДУ https:// и .bitrix24.ru')

    api_user_id = models.IntegerField(verbose_name='Bitrix24 API USER ID')
    api_key = models.CharField(max_length=150, verbose_name='Bitrix24 API KEY (входящий веб-хук в Bitrix24)')
    secret_token = models.CharField(max_length=150,
                                    verbose_name='Токен приложения Bitrix24 (исходящий веб-хук в Bitrix24)')
    save_all_portal_tasks = models.BooleanField(verbose_name="Сохранять ВСЕ данные всех задач портала в БД Django",
                                                help_text="Внимание! Данная настройка "
                                                          "позволяет сохранять ВСЕ данные ВСЕХ задач"
                                                          "портала в БД! Рекомендуется использовать ТОЛЬКО для "
                                                          "основного портала BX24! Если вам необходимо сохранять "
                                                          "ОБЩИЕ задачи двух порталов, установите соответствующую"
                                                          " галочку в настройках коннектора, а не здесь!!",
                                                default=False)
    write_logs = models.BooleanField(verbose_name="Писать логи портала",
                                     help_text="при включенной настройке сохранения всех задач",
                                     default=False)
    related_task_url_field = models.CharField(max_length=150, null=True, blank=True,
                                              verbose_name="Поле в BX24 для записи URL связанной задачи",
                                              help_text="Оставьте пустым, если не хотите, чтобы в bitrix передавался URL"
                                                        "связанного портала"
                                              )

    def get_bitrix_url(self):
        return f'https://{self.hostname}.bitrix24.ru/'

    def import_groups_from_bitrix(self):
        bx = BitrixConnector(self)
        groups = bx.get_groups()

        for g in groups:
            GroupBitrix.objects.update_or_create(
                bitrix_id=g['ID'],
                bitrix_portal=self,
                defaults={
                    'title': g['NAME'],
                    'bitrix_portal': self
                },
            )

    def import_users_from_bitrix(self):
        bx = BitrixConnector(self)
        users = bx.get_users()

        for u in users:
            UserBitrix.objects.update_or_create(
                bitrix_id=u['ID'],
                bitrix_portal=self,
                defaults={
                    'name': u['NAME'],
                    'second_name': u['SECOND_NAME'],
                    'last_name': u['LAST_NAME'],
                    'bitrix_id': u['ID'],
                    'bitrix_portal': self
                },
            )

    def import_tasks_from_bitrix(self, group_id):
        bx = BitrixConnector(self)
        tasks = bx.get_tasks(group_id)
        if self.save_all_portal_tasks:
            for task in tasks:
                self.add_task_to_django(task)

    def add_task_to_django(self, bitrix_task, custom_group_id: str = ""):
        dj_task = get_or_create_main_task(self, bitrix_task)
        dj_task.update_data_from_bitrix_task(bitrix_task)
        dj_task.save()
        return dj_task

    def get_absolute_url(self):
        return reverse('portal-detail', args=[str(self.pk)])

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Портала Bitrix24"
        verbose_name_plural = 'Порталы Bitrix24'


class UserBitrix(models.Model):
    name = models.CharField(max_length=150, null=True, blank=True, verbose_name="Имя")
    second_name = models.CharField(max_length=150, null=True, blank=True, verbose_name="Фамилия")
    last_name = models.CharField(max_length=150, null=True, blank=True, verbose_name="Отчество")
    bitrix_id = models.IntegerField(verbose_name='ID пользователя в Bitrix24', blank=True, null=True)
    bitrix_portal = models.ForeignKey(BitrixPortal, on_delete=models.CASCADE, verbose_name='Bitrix24 пользователя')

    def get_admin_url(self):
        return reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name),
                       args=[self.id])

    def __str__(self):
        return f'{self.last_name} {self.name} из {self.bitrix_portal}'

    @staticmethod
    def get_or_create_custom(portal, bitrix_id):
        return UserBitrix.objects.get_or_create(bitrix_id=bitrix_id,
                                                bitrix_portal=portal,
                                                defaults={
                                                    'name': "Создан автоматически",
                                                    'last_name': 'Импортируйте пользователей из BX24 для',
                                                }
                                                )[0]

    class Meta:
        verbose_name = "Пользователь Bitrix24"
        verbose_name_plural = "Пользователи Bitrix24"


class GroupBitrix(models.Model):
    title = models.CharField(max_length=150, null=True, blank=True, verbose_name="Название группы")
    bitrix_id = models.IntegerField(verbose_name='ID в Bitrix24', blank=True, null=True)
    bitrix_portal = models.ForeignKey(BitrixPortal, on_delete=models.CASCADE, verbose_name='Bitrix24 пользователя')

    def get_admin_url(self):
        return reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name),
                       args=[self.id])

    def get_bitrix_url(self):
        return f'{self.bitrix_portal.get_bitrix_url()}workgroups/group/{self.bitrix_id}/'

    @admin.display(description="Ссылка на группу в Bitrix24")
    def show_bitrix_url(self):
        return format_html("<a href='{url}'>{url}</a>", url=self.get_bitrix_url())

    def get_tasks_count(self):
        return self.bitrixtaskkeeper_set.count()

    def __str__(self):
        return f'{self.title} из {self.bitrix_portal}'

    class Meta:
        verbose_name = "Группа портала Bitrix24"
        verbose_name_plural = "Группы портала Bitrix24"


class Task(models.Model):
    title = models.CharField(max_length=250, blank=True, verbose_name="Название задачи", null=True)
    description = models.TextField(verbose_name="Описание", blank=True, null=True)

    created_at = models.DateTimeField(verbose_name="Дата создания задачи", blank=True, null=True)
    deadline = models.DateTimeField(verbose_name="Дедлайн", blank=True, null=True)

    start_date_plan = models.DateTimeField(verbose_name="Запланированная дата начала", blank=True, null=True)
    time_estimate = models.DurationField(verbose_name="Трудозатраты, план", blank=True, null=True)
    closed_date = models.DateTimeField(verbose_name="Дата завершения задачи", blank=True, null=True)

    status_choices = [
        ('2', 'Ждет выполнения'),
        ('3', 'Выполняется'),
        ('4', 'Ожидает контроля'),
        ('5', 'Завершена'),
        ('6', 'Отложена'),
    ]

    status = models.CharField(max_length=4, choices=status_choices, default='2')

    def get_admin_url(self):
        return reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name),
                       args=[self.id])

    def __str__(self):
        return f'{self.title}'

    def update_or_create_task_keeper(self, portal, bitrix_task):
        BitrixTaskKeeper.objects.update_or_create(
            django_task=self,
            portal=portal,
            bitrix_id=bitrix_task['id'],
            defaults={
                'group': GroupBitrix.objects.get_or_create(bitrix_portal=portal, bitrix_id=bitrix_task['groupId'])[0],
                'createdBy': UserBitrix.get_or_create_custom(portal, bitrix_task['createdBy']),
                'responsible': UserBitrix.get_or_create_custom(portal, bitrix_task['responsibleId']),
            },
        )

    def update_data_from_bitrix_task(self, bitrix_task):
        """
        Принимает задачу из Bitrix24
        Преобразует в модель Task
        и создает BitrixTaskKeeper
        """

        self.title = bitrix_task['title']
        self.description = bitrix_task['description']
        self.status = bitrix_task['status']
        self.created_at = parse_datetime(bitrix_task['createdDate'])

        if 'deadline' in bitrix_task and bitrix_task['deadline']:
            self.deadline = parse_datetime(bitrix_task['deadline'])

        if 'startDatePlan' in bitrix_task and bitrix_task['startDatePlan']:
            self.start_date_plan = parse_datetime(bitrix_task['startDatePlan'])

        if 'timeEstimate' in bitrix_task and bitrix_task['timeEstimate']:
            self.time_estimate = parse_duration(bitrix_task['timeEstimate'])

        if 'closedDate' in bitrix_task and bitrix_task['closedDate']:
            self.closed_date = parse_datetime(bitrix_task['closedDate'])

    class Meta:
        verbose_name = "Задача"
        verbose_name_plural = 'Задачи'
        ordering = ['-created_at']


class BitrixTaskKeeper(models.Model):
    """Класс, хранящий ID задачи в определенном портале BX24"""
    django_task = models.ForeignKey(Task, on_delete=models.CASCADE, verbose_name="Главная задача",
                                    related_name="related")

    portal = models.ForeignKey(BitrixPortal, on_delete=models.PROTECT, verbose_name="Портал BX24")
    bitrix_id = models.IntegerField(verbose_name="ID задачи в BX24", null=True, blank=True)

    group = models.ForeignKey(GroupBitrix, on_delete=models.SET_NULL, verbose_name='Группа в Bitrix24',
                              blank=True, null=True)

    createdBy = models.ForeignKey(UserBitrix, on_delete=models.SET_NULL, verbose_name='Постановщик',
                                  blank=True, null=True, related_name='task_creator')
    responsible = models.ForeignKey(UserBitrix, on_delete=models.SET_NULL, verbose_name='Ответственный',
                                    blank=True, null=True, related_name='task_responsible')

    auditors = models.ManyToManyField(UserBitrix, verbose_name="Наблюдатели", related_name='+', blank=True)
    accomplices = models.ManyToManyField(UserBitrix, verbose_name="Соисполнители", related_name='+', blank=True)

    updated_at = models.DateTimeField(auto_now=True, verbose_name="Дата и время последнего обновления задачи")

    def get_admin_url(self):
        return reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name),
                       args=[self.id])

    def add_task_to_bitrix_portal(self):
        bx = BitrixConnector(self.bitrix_portal)
        bx_task = bx.add_task_to_bitrix_portal(self)
        self.bitrix_id = bx_task['id']

    def get_task_from_bitrix(self):
        bx = BitrixConnector(self.bitrix_portal)
        bx_task = bx.get_task_from_bitrix(self.bitrix_id)
        return bx_task

    def get_bitrix_url(self):
        return f'{self.portal.get_bitrix_url()}company/personal/user/1/tasks/task/view/{self.bitrix_id}/'

    def get_auditors_ids(self):
        return [auditor.bitrix_id for auditor in self.auditors.all()]

    def get_accomplices_ids(self):
        return [accomplice.bitrix_id for accomplice in self.accomplices.all()]

    @admin.display(description="Ссылка на задачу в Bitrix24")
    def show_bitrix_url(self):
        return format_html("<a href='{url}'>{url}</a>", url=self.get_bitrix_url())

    @staticmethod
    def get_keeper(portal, task_id):
        return BitrixTaskKeeper.objects.filter(bitrix_id=task_id,
                                               portal=portal).first()

    def __str__(self):
        return f'"{self.django_task}" из "{self.portal}"'


class Comment(models.Model):
    message = models.TextField(verbose_name="Комментарий", null=True, blank=True)
    django_task = models.ForeignKey(Task, on_delete=models.CASCADE, verbose_name="Главная задача")

    def update_or_create_comment_keeper(self, task_keeper, comment_id):
        BitrixCommentKeeper.objects.update_or_create(
            bitrix_task_keeper=task_keeper,
            bitrix_id=comment_id,
            main_comment=self,
            defaults={
                'author': task_keeper.createdBy,
            },
        )

    def update_or_create_comment_in_bitrix(self, portal, task_keeper, bitrix_comment):
        bx = BitrixConnector(portal)
        comment_keeper = BitrixCommentKeeper.objects.filter(main_comment=self, bitrix_task_keeper=task_keeper).first()
        if not comment_keeper:
            comment_id = bx.add_comment(task_keeper.bitrix_id, task_keeper.createdBy.bitrix_id,
                                        convert_comment_message(bitrix_comment))
        else:
            time_beetween_updates = timezone.now() - comment_keeper.updated_at
            if time_beetween_updates.total_seconds() < 5: return
            comment_id = bx.update_comment(task_keeper.bitrix_id,
                                           comment_keeper.bitrix_id, convert_comment_message(bitrix_comment))
        self.update_or_create_comment_keeper(task_keeper, comment_id)


class BitrixCommentKeeper(models.Model):
    bitrix_task_keeper = models.ForeignKey(BitrixTaskKeeper, on_delete=models.CASCADE, verbose_name="Bitrix Task Keeper")
    bitrix_id = models.IntegerField(verbose_name="ID комментария в BX24", null=True, blank=True)
    author = models.ForeignKey(UserBitrix, null=True, blank=True, on_delete=models.CASCADE,
                               verbose_name="Автор комментария")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Дата и время последнего обновления комментария")
    main_comment = models.ForeignKey(Comment, on_delete=models.CASCADE, verbose_name="Главный комментарий")


class ElapsedTime(models.Model):
    seconds = models.IntegerField(verbose_name="Затраченное время в секундах", null=True, blank=True)
    django_task = models.ForeignKey(Task, on_delete=models.CASCADE, verbose_name="Главная задача")
    created_date = models.DateTimeField(blank=True, null=True)
    date_start = models.DateTimeField(blank=True, null=True)
    date_stop = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(verbose_name="Комментарий", null=True, blank=True)
    author = models.CharField(max_length=100, verbose_name="Имя пользователя, добавившего время", null=True, blank=True)

    def update_or_create_time_keeper(self, task_keeper, time_id):
        return BitrixElapsedTimeKeeper.objects.update_or_create(
            bitrix_task_keeper=task_keeper,
            bitrix_id=time_id,
            main_time=self,
        )[0]

    def update_or_create_time_in_bitrix(self, task_keeper):
        fields = self.get_time_fields_for_bitrix(task_keeper)

        bx = BitrixConnector(task_keeper.portal)
        time_keeper = BitrixElapsedTimeKeeper.objects.filter(main_time=self, bitrix_task_keeper=task_keeper).first()

        if not time_keeper:
            time_id = bx.add_elapsed_time(task_keeper.bitrix_id, fields)
        else:
            if not self.should_update_time(time_keeper):
                return
            time_id = bx.update_elapsed_time(task_keeper.bitrix_id,
                                           time_keeper.bitrix_id, fields)
        time_keeper = self.update_or_create_time_keeper(task_keeper, time_id)
        time_keeper.seconds = self.seconds
        time_keeper.save()

    @staticmethod
    def should_update_time(time_keeper):
        if time_keeper.seconds == time_keeper.main_time.seconds:
            return False
        time_beetween_updates = timezone.now() - time_keeper.updated_at
        if time_beetween_updates.total_seconds() < 10:
            return False

        return True

    def get_time_fields_for_bitrix(self, task_keeper):
        created_date = self.created_date
        date_start = self.date_start
        date_stop = self.date_stop
        fields = {
            'SECONDS': self.seconds,
            'USER_ID': task_keeper.createdBy.bitrix_id,
            'CREATED_DATE': created_date.isoformat(),
            'DATE_START': date_start.isoformat(),
            'DATE_STOP': date_stop.isoformat(),
            'COMMENT_TEXT': f'Добавил:{self.author};{self.comment}'
        }
        return fields

    def delete_time_from_all_portals(self):
        related_time = self.bitrixelapsedtimekeeper_set.all()
        for time in related_time:
            time.delete_from_bitrix()


class BitrixElapsedTimeKeeper(models.Model):
    bitrix_task_keeper = models.ForeignKey(BitrixTaskKeeper, on_delete=models.CASCADE, verbose_name="Bitrix Task Keeper")
    bitrix_id = models.IntegerField(verbose_name="item ID в BX24", null=True, blank=True)
    author = models.ForeignKey(UserBitrix, null=True, blank=True, on_delete=models.CASCADE,
                               verbose_name="Пользователь, затративший время")
    seconds = models.IntegerField(verbose_name="Затраченное время в секундах", null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Дата и время последнего"
                                                                  " обновления затраченного времени")
    main_time = models.ForeignKey(ElapsedTime, on_delete=models.CASCADE, verbose_name="Главное время")

    def delete_from_bitrix(self):
        bx = BitrixConnector(self.bitrix_task_keeper.portal)
        try:
            bx.delete_elapsed_time(self.bitrix_task_keeper.bitrix_id, self.bitrix_id)
        except:
            pass


class TaskExchanger(models.Model):
    """
    Класс, в котором находятся настройки "обменника" задачами
    из какого портала в какой кидать задачи (или же обоюдно)
    и тп
    """
    bitrix_1 = models.ForeignKey(BitrixPortal, on_delete=models.PROTECT,
                                 verbose_name="Портал 1",
                                 related_name='exchanger_bitrix_1')

    group_1 = models.ForeignKey(GroupBitrix, on_delete=models.PROTECT,
                                verbose_name="Группа для отслеживания задач портала 1",
                                related_name='exchanger_group_1'
                                )

    responsible_1 = models.ForeignKey(UserBitrix, on_delete=models.PROTECT,
                                      verbose_name="Ответственный для задач портала 1",
                                      help_text="Он будет указан отвественным для задач, поставленных из второго портала",
                                      related_name="exchanger_responsible_1"
                                      )

    auditors_1 = models.ManyToManyField(UserBitrix,
                                        verbose_name="Наблюдатели для задач портала 1",
                                        related_name="exchanger_auditors_1",
                                        blank=True
                                        )

    bitrix_2 = models.ForeignKey(BitrixPortal, on_delete=models.PROTECT,
                                 verbose_name="Портал 2",
                                 related_name='exchanger_bitrix_2'
                                 )

    group_2 = models.ForeignKey(GroupBitrix, on_delete=models.PROTECT,
                                verbose_name="Группа для отслеживания задач портала 2",
                                related_name='exchanger_group_2')

    responsible_2 = models.ForeignKey(UserBitrix, on_delete=models.PROTECT,
                                      verbose_name="Ответственный для задач портала 2",
                                      help_text="Он будет указан отвественным для задач, поставленных из первого портала",
                                      related_name="exchanger_responsible_2",
                                      )

    auditors_2 = models.ManyToManyField(UserBitrix,
                                        verbose_name="Наблюдатели для задач портала 2",
                                        related_name="exchanger_auditors_2",
                                        blank=True
                                        )

    save_shared_tasks = models.BooleanField(verbose_name="Сохранять данные общих задач в БД Django",
                                            help_text="Необязательная настройка, не устанавливайте галочку,"
                                                      " если вы не знаете, что делаете")

    write_logs = models.BooleanField(verbose_name="Писать логи?", default=False,
                                     help_text="Включайте при необходимости, чтобы не забивать БД")

    enabled = models.BooleanField(verbose_name="Включить коннектор", default=True,
                                  help_text="Позволяет быстро включить/отключить"
                                            " коннектор связей задач BX24 без необходимости"
                                            " удалять настройку")

    def write_exchanger_logic_to_task_keeper(self, portal: BitrixPortal, task: BitrixTaskKeeper):
        self.write_main_logic_to_task_keeper(portal, task)
        self.write_custom_logic_to_task_keeper(portal, task)

    def write_main_logic_to_task_keeper(self, portal: BitrixPortal, task: BitrixTaskKeeper):
        logic = self.get_portal_logic(portal)
        task.portal = portal
        task.group = logic['group']
        task.responsible = logic['responsible']
        task.createdBy = logic['createdBy']
        task.auditors.set(logic['auditors'].all())

    def write_custom_logic_to_task_keeper(self, portal: BitrixPortal, task: BitrixTaskKeeper):
        logics = self.get_custom_logic(portal)
        for logic in logics:
            if logic.title_contains.lower() in task.django_task.title.lower():
                if logic.responsible:
                    task.responsible = logic.responsible
                if logic.auditors.count() > 0:
                    task.auditors.set(logic.auditors.all())
                if logic.accomplices.count() > 0:
                    task.accomplices.set(logic.accomplices.all())

    def get_custom_logic(self, portal: BitrixPortal):
        return CustomLogic.objects.filter(portal=portal, exchanger=self)

    def get_portal_logic(self, portal):
        if portal == self.bitrix_1:
            return {
                "group": self.group_1,
                "portal": portal,
                'responsible': self.responsible_1,
                'createdBy': self.responsible_1,
                'auditors': self.auditors_1,
            }
        elif portal == self.bitrix_2:
            return {
                "group": self.group_2,
                "portal": portal,
                'responsible': self.responsible_2,
                'createdBy': self.responsible_2,
                'auditors': self.auditors_2,
            }

    def get_second_portal(self, portal_1):
        if portal_1 == self.bitrix_1:
            return self.bitrix_2
        return self.bitrix_1

    def __str__(self):
        return f'Коннектор "{self.bitrix_1}" и "{self.bitrix_2}"'

    class Meta:
        verbose_name = "Настройки межпортальных задач"
        verbose_name_plural = "Настройки межпортальных задач"


class ExchangeLogger(models.Model):
    """ Класс логирования обмена """
    exchanger = models.ForeignKey(TaskExchanger, on_delete=models.CASCADE, verbose_name="Используемая настройка",
                                  related_name="logger_exchanger", blank=True, null=True)
    time = models.DateTimeField(auto_now_add=True, verbose_name="Время события")
    description = models.CharField(max_length=200, verbose_name="Описание события")

    portal = models.ForeignKey(BitrixPortal, on_delete=models.CASCADE, verbose_name="Используемая настройка",
                               related_name="logger_exchanger", blank=True, null=True)

    quality_choices = [
        ('GOOD', 'SUCCESS'),
        ('BAD', 'ERROR'),
        ('OK', 'OK'),
    ]

    quality = models.CharField(
        max_length=4,
        choices=quality_choices,
        default='OK',
    )

    @staticmethod
    def connector_task_work_done(connector, create_task_id):
        if connector.write_logs:
            ExchangeLogger.objects.create(exchanger=connector,
                                          description=f'Работа завершена, задача добавлена/обновлена '
                                                      f'(id созданной/обновленной задачи:{create_task_id}')

    @staticmethod
    def connector_founded(connector):
        if connector.write_logs:
            ExchangeLogger.objects.create(exchanger=connector, quality="GOOD",
                                          description=f'Коннектор "{connector}" найден, начинает работу')

    @staticmethod
    def custom_message(connector, message, quality="OK"):
        if connector.write_logs:
            ExchangeLogger.objects.create(exchanger=connector, quality=quality,
                                          description=message)

    @staticmethod
    def custom_message_portal(portal, message, quality="OK"):
        if portal.write_logs:
            ExchangeLogger.objects.create(portal=portal, quality=quality,
                                          description=message)


class CustomLogic(models.Model):
    exchanger = models.ForeignKey(TaskExchanger, on_delete=models.CASCADE, verbose_name="Настройка обмена с задачами")

    title_contains = models.CharField(max_length=50, verbose_name="Если заголовок задачи содержит")

    portal = models.ForeignKey(BitrixPortal, on_delete=models.CASCADE, verbose_name="Портал",
                               help_text="В него будут записаны изменения")

    responsible = models.ForeignKey(UserBitrix, verbose_name="Назначить ответственного",
                                    on_delete=models.PROTECT,
                                    blank=True,
                                    null=True)

    auditors = models.ManyToManyField(UserBitrix, verbose_name="Назначить наблюдателей",
                                      related_name="custom_auditors",
                                      blank=True)

    accomplices = models.ManyToManyField(UserBitrix, verbose_name="Назначить соисполнителей",
                                         related_name="custom_accomplices",
                                         blank=True)

    def __str__(self):
        return f'Если в заголовке "{self.title_contains}", портал {self.portal}'

    class Meta:
        verbose_name = "Переназначение сотрудников"
        verbose_name_plural = "Переназначение сотрудников"


def get_or_create_main_elapsed_time(task_keeper_1, bx_time):
    """
    Создает main elapsed time
    """
    author = get_user_name(task_keeper_1.portal, bx_time['USER_ID'])
    time_keeper = BitrixElapsedTimeKeeper.objects.filter(bitrix_task_keeper=task_keeper_1, bitrix_id=bx_time['ID']).first()

    if time_keeper:
        ElapsedTime.objects.filter(pk=time_keeper.main_time.pk).update(seconds=bx_time['SECONDS'], created_date=bx_time['CREATED_DATE'],
                                   date_start=bx_time['DATE_START'], date_stop=bx_time['DATE_STOP'],
                                   comment=bx_time['COMMENT_TEXT'], author=author)
        return time_keeper.main_time



    main_time = ElapsedTime.objects.create(django_task=task_keeper_1.django_task, seconds=bx_time['SECONDS'],
                                           created_date=bx_time['CREATED_DATE'], date_start=bx_time['DATE_START'],
                                           date_stop=bx_time['DATE_STOP'], comment=bx_time['COMMENT_TEXT'], author=author)
    main_time.full_clean()
    main_time.update_or_create_time_keeper(task_keeper_1, bx_time['ID'])

    return main_time


def get_or_create_main_task(portal, bitrix_task, event: str):
    """
    Создает main задачу и task keeper
    """
    keeper = BitrixTaskKeeper.objects.filter(bitrix_id=bitrix_task['id'],
                                             portal=portal).first()
    if keeper:
        return keeper.django_task

    if event == 'ONTASKADD':
        main_task = Task.objects.create(title=bitrix_task['title'])
        main_task.update_or_create_task_keeper(portal, bitrix_task)
        return main_task

    return False



def get_or_create_main_comment(task_keeper_1, comment_id):
    """
    Создает main comment
    """
    comment_keeper = BitrixCommentKeeper.objects.filter(bitrix_task_keeper=task_keeper_1, bitrix_id=comment_id).first()
    if comment_keeper:
        return comment_keeper.main_comment

    main_comment = Comment.objects.create(django_task=task_keeper_1.django_task)

    main_comment.update_or_create_comment_keeper(task_keeper_1, comment_id)

    return main_comment


def convert_comment_message(bitrix_comment):
    if "Автор: " in bitrix_comment["POST_MESSAGE"]:
        return bitrix_comment["POST_MESSAGE"]
    else:
        return f'Автор: {bitrix_comment["AUTHOR_NAME"]} <br>' \
                               f' Комментарий: {bitrix_comment["POST_MESSAGE"]}'

def get_user_name(portal, user_id):
    """
    Возвращает строковое представление пользователя, либо, если не найден, строку вида 'ID №{user id}
    :param portal:
    :param user_id:
    :return:
    """
    user = UserBitrix.objects.filter(bitrix_portal=portal, bitrix_id=user_id).first()
    if user:
        return str(user)
    else:
        return f'ID {user_id}'