import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView

from .models import BitrixPortal, TaskExchanger, Task, ExchangeLogger, BitrixTaskKeeper, get_or_create_main_task, \
    Comment, get_or_create_main_comment, get_or_create_main_elapsed_time
from bitrix_connector.main import BitrixConnector
from django.http import Http404
import time
import logging
# Create your views here.


@csrf_exempt
def income_webhook(request, hostname):
    """
    Обработка исходящего из Bitrix24 вебхука
    """

    logger = logging.getLogger("bitrix_integrator")
    logger.debug(f"Получен запрос {str(request.POST)}")
    auth_token = str(request.POST.get('auth[application_token]'))
    settings = {
        'portal_1': get_object_or_404(BitrixPortal, hostname=hostname),
        'event': str(request.POST.get('event'))
    }

    if auth_token != settings['portal_1'].secret_token:
        logger.debug(f"Присланный секретный токен не совпал!")
        raise Http404

    events_functions = get_events_dictionary()

    if settings['event'] in events_functions:
        events_functions[settings['event']](request, settings, logger)

    return HttpResponse(settings['portal_1'])


def get_events_dictionary():
    return {
        'ONTASKADD': income_task_processing,
        'ONTASKUPDATE': income_task_processing,
        'ONTASKCOMMENTADD': income_comment_add_processing,
        'ONTASKCOMMENTUPDATE': income_comment_add_processing,
    }


def income_comment_add_processing(request, settings, logger):
    are_settings_ok = get_initial_comment_settings(request, settings)
    if not are_settings_ok:
        return "Коммент не подходит под условия"

    main_comment = get_or_create_main_comment(settings['task_keeper_1'], settings['bx1_comment_id'])

    # ищем коннекторы портала
    connectors = get_relevant_connectors(settings['portal_1'], int(settings['bx1_task']["groupId"]))
    logger = logging.getLogger(__name__)
    logger.debug("TEST!")
    if len(connectors) > 0:
        for connector in connectors:
            ExchangeLogger.custom_message(connector, f'Коннектор найден. Начинаем добавление/изменение комментария'
                                                     f' №{settings["bx1_comment_id"]}')
            portal_2 = connector.get_second_portal(settings['portal_1'])
            task_keeper_2 = BitrixTaskKeeper.objects.filter(portal=portal_2,
                                                            django_task=settings['task_keeper_1'].django_task).first()
            if not task_keeper_2:
                ExchangeLogger.custom_message(connector, "Не найден кипер задачи, пропускаем")
                continue

            main_comment.update_or_create_comment_in_bitrix(
                portal_2, task_keeper_2, settings['bx1_comment'])

    elif settings['portal_1'].save_all_portal_tasks:
        ExchangeLogger.custom_message_portal(settings['portal_1'],
                                             f'Установлено сохранение всех задач портала,'
                                             f' сохраняем коммент {settings["bx1_task_id"]}')
        main_comment.message = f'Автор: {settings["bx1_comment"]["AUTHOR_NAME"]} <br>' \
                               f' Комментарий: {settings["bx1_comment"]["POST_MESSAGE"]}'

    return "ok"


def get_initial_comment_settings(request, settings):
    settings['bx1_task_id'] = str(request.POST.get('data[FIELDS_AFTER][TASK_ID]'))
    settings['bx1_comment_id'] = str(request.POST.get('data[FIELDS_AFTER][ID]'))
    settings['task_keeper_1'] = BitrixTaskKeeper.objects.filter(portal=settings['portal_1'],
                                                  bitrix_id=settings['bx1_task_id']).first()
    if not settings['task_keeper_1']:
        return False

    settings['bx1_task'] = BitrixConnector(settings['task_keeper_1'].portal).get_task_from_bitrix(settings['bx1_task_id'])
    settings['bx1_comment'] = BitrixConnector(settings['task_keeper_1'].portal).get_comment(settings['bx1_task_id'],
                                                                                             settings['bx1_comment_id'])

    bad_comments = ['Крайний срок изменен', 'Задача просрочена.', 'задача просрочена, эффективность снижена.',
                   'Задача почти просрочена', 'задача почти просрочена','вы назначены постановщиком', 'вы назначены ответственным.',
                   'вы добавлены наблюдателем.', 'Задача завершена.', 'задача возвращена в работу.']

    for comment in bad_comments:
        if comment in settings['bx1_comment']["POST_MESSAGE"]:
            return False

    return True


def income_task_processing(request, settings, logger):
    settings['bx1'] = BitrixConnector(settings['portal_1'])  # инициализируем коннектор, передаем портал битрикса
    bx1_task_id = str(request.POST.get('data[FIELDS_AFTER][ID]'))  # берем ID задачи из запроса
    settings['bx1_task'] = settings['bx1'].get_task_from_bitrix(bx1_task_id)  # получаем задачу из битрикса

    # ищем коннекторы портала
    connectors = get_relevant_connectors(settings['portal_1'], int(settings['bx1_task']["groupId"]))
    logger.debug(f"Найденные коннекторы: {str(connectors)}")
    if len(connectors) > 0:
        for connector in connectors:
            ExchangeLogger.connector_founded(connector)
            settings['connector'] = connector
            settings['portal_2'] = connector.get_second_portal(settings['portal_1'])
            logger.debug(f"Обработка коннектора {str(connector)}, засыпаем на 10 секунд")
            time.sleep(10)
            update_task_from_portal1_in_portal2(settings, logger)
    elif settings['portal_1'].save_all_portal_tasks:
        ExchangeLogger.custom_message_portal(settings['portal_1'],
                                             f'Установлено сохранение всех задач портала,'
                                             f' создаем задачу {settings["bx1_task"]["id"]}')
        main_task = get_or_create_main_task(settings['portal_1'], settings['bx1_task'])
        main_task.update_data_from_bitrix_task(settings['bx1_task'])
    return "ok"


def update_task_from_portal1_in_portal2(settings, logger):
    logger.debug(f"Коннектор {settings['connector']}, ищем главную задачу")
    logger.debug(f"Первая задача - {settings['bx1_task']}")
    main_task = get_or_create_main_task(settings['portal_1'], settings['bx1_task'], settings['event'])
    if not main_task:
        return
    bx2 = BitrixConnector(settings['portal_2'])
    update_or_add = bx2.update_task
    #  ищем кипер портала 2, если не было, создаем новую задачу,
    bx2_task_keeper = main_task.related.filter(portal=settings['portal_2']).first()

    if bx2_task_keeper:
        time_beetween_updates = timezone.now() - bx2_task_keeper.updated_at
        if time_beetween_updates.total_seconds() < 60:
            return
    else:
        logger.debug("Похоже, задача новая, будем создавать во втором портале")
        bx2_task_keeper = BitrixTaskKeeper.objects.create(django_task=main_task, portal=settings['portal_2'])
        update_or_add = bx2.add_bx_task_to_bitrix

    # Обновляем логику коннектора в кипере
    settings['connector'].write_exchanger_logic_to_task_keeper(settings['portal_2'], bx2_task_keeper)
    ExchangeLogger.custom_message(settings['connector'], "Соединяемся с порталом 2 для создания/обновления задачи")
    bx2_task = update_or_add(settings['bx1_task'], bx2_task_keeper)
    logger.debug(f"Вторая задача - {bx2_task}")
    bx2_task_keeper.bitrix_id = bx2_task['id']
    bx2_task_keeper.save()

    # пишем related_field_id
    # потом переписать -- копипаста
    if (settings['portal_1'].related_task_url_field):
        settings['bx1'].update_task_custom_fields(
            settings['bx1_task']['id'], {
                settings['portal_1'].related_task_url_field: bx2_task_keeper.get_bitrix_url()
            })

    if (settings['portal_2'].related_task_url_field):
        bx1_task_keeper = BitrixTaskKeeper.get_keeper(settings['portal_1'], settings['bx1_task']['id'])
        bx2.update_task_custom_fields(
            bx2_task['id'], {
                settings['portal_2'].related_task_url_field: bx1_task_keeper.get_bitrix_url()
            })

    # сохраняем данные задачи в БД при необходимости
    if settings['connector'].save_shared_tasks or settings['portal_1'].save_all_portal_tasks \
            or settings['portal_2'].save_all_portal_tasks:
        main_task.update_data_from_bitrix_task(bx2_task)

    # обновляем время при необходимости
    if settings['bx1_task']['timeSpentInLogs'] != bx2_task['timeSpentInLogs']:
        keeper_1 = BitrixTaskKeeper.get_keeper(settings['portal_1'], settings['bx1_task']['id'])
        update_elapsed_time(keeper_1, bx2_task_keeper)

    ExchangeLogger.connector_task_work_done(settings['connector'], bx2_task_keeper.bitrix_id)

    return 'ok'


def get_relevant_connectors(portal_1, task_group_id):
    """
    Возвращает все подходящие коннекторы портала
    :param portal_1:
    :param task_group_id:
    :return:
    """
    connectors = TaskExchanger.objects.filter(Q(bitrix_1=portal_1) | Q(bitrix_2=portal_1))
    relevant_connectors = []
    for connector in connectors:
        if not connector.enabled:
            continue

        logic = connector.get_portal_logic(portal_1)
        # если группа в настройках коннектора совпадает с группой задачи, тогда добавляем или обновляем коммент
        if logic['group'].bitrix_id == task_group_id:
            relevant_connectors.append(connector)

    return relevant_connectors


def update_elapsed_time(task_keeper_1, task_keeper_2):
    bx_1 = BitrixConnector(task_keeper_1.portal)
    all_time = bx_1.get_elapsed_time(task_keeper_1.bitrix_id)
    already_added_time = task_keeper_1.django_task.elapsedtime_set.all()
    # Добавляем время
    for bx_time in all_time:
        main_time = get_or_create_main_elapsed_time(task_keeper_1, bx_time)
        main_time.update_or_create_time_in_bitrix(task_keeper_2)
        already_added_time = already_added_time.exclude(pk=main_time.pk)
    # Удаляем время
    for time in already_added_time:
        time.delete_time_from_all_portals()
        time.delete()


class BitrixIncomeError(ValueError):
    def __init__(self, code, description):
        super().__init__(description)
        self.code = code

    def __str__(self):
        return f'Ошибка при обмене задач BX24, код: {self.code}. Описание: {self.description}'


class PortalDetailView(DetailView):
    """
    Контроллер подробностей о портале
    """
    model = BitrixPortal
    context_object_name = 'portal'

    def get_context_data(self, **kwargs):
        context = super(PortalDetailView, self).get_context_data(**kwargs)
        context['status_colors'] = self.get_status_colors()
        return context

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(PortalDetailView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_status_colors():
        return {
            '2': '',
            '3': '#3498db',
            '4': '#f1c40f',
            '5': '#2ecc71',
            '6': '#9b59b6',
        }

