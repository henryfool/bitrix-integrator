from django.urls import path
from users.views import *

urlpatterns = [
    path('login/', CustomLogin.as_view(), name='login'),
    path('logout/', custom_logout, name='logout'),
    path('users/<int:pk>/edit/', CustomUpdateProfile.as_view(), name='edit-user-info'),
]