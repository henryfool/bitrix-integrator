from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, Group
from django.db import models

from .managers import CustomUserManager


class CustomUser(AbstractUser):
    """Custom User model."""

    username = None
    email = models.EmailField(unique=True)

    name = models.CharField(max_length=15, verbose_name='Имя')
    last_name = models.CharField(max_length=25, verbose_name='Фамилия')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


UserModel = get_user_model()
