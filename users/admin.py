from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('email', 'name', 'last_name', 'is_staff', 'is_active')
    list_filter = ('email', 'name', 'last_name', 'is_staff', 'is_active')

    fieldsets = (
        ('None', {
            'fields': ('email', 'name', 'last_name', 'password', 'is_active')
        }),
    )

    add_fieldsets = (
        (None, {
            'fields': ('email', 'name', 'last_name', 'password1', 'password2'),
        }
        ),
    )

    search_fields = ('email',)
    ordering = ('email',)


admin.site.register(CustomUser, CustomUserAdmin)
