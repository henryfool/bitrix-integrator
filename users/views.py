from django.contrib.auth import logout
from django.contrib.auth.views import LoginView
from django.views.generic import UpdateView
from .models import CustomUser
from django.shortcuts import redirect

from .forms import CustomUserAuthenticationForm, CustomUserChangeForm


class CustomLogin(LoginView):
    template_name = 'users/login.html'
    authentication_form = CustomUserAuthenticationForm


class CustomUpdateProfile(UpdateView):
    model = CustomUser
    template_name = 'users/change_user_info.html'
    authentication_form = CustomUserChangeForm
    fields = ['email', 'name', 'last_name']
    success_url = ''


def custom_logout(request):
    logout(request)
    return redirect('login')
